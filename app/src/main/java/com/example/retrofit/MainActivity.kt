package com.example.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.retrofit.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val list = ArrayList<PostResponse>()
    private val commentList = ArrayList<CommentResponse>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding){
            rvRespnse.setHasFixedSize(true)
            rvRespnse.layoutManager = LinearLayoutManager(this@MainActivity)
        }
        //createPost()
        //showComments()
        //getComment(3)
        //getUserPost(2)
        //updatePost()
        deletePost(2)
    }

    private fun deletePost(postNumberToDelete: Int){
        RetrofitClient.instance.deletePost(postNumberToDelete).enqueue(object: Callback<Void>{
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.code() == 200)
                    binding.tvResponseCode.text = getString(R.string.resource_delete_success)
                else
                    binding.tvResponseCode.text = getString(R.string.resource_not_delete, "")
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                binding.tvResponseCode.text = getString(R.string.resource_not_delete, t.message)
            }
        })
    }

    private fun updatePost(){
        RetrofitClient.instance.patchPost(5, 4, 5, "Some title",
            "some text for the PUT request").enqueue(object: Callback<PostResponse>{
            override fun onResponse(call: Call<PostResponse>, response: Response<PostResponse>) {
                binding.tvResponseCode.text = response.code().toString()
                try {
                    val responseText = resources.getString(R.string.create_post_response, response.code().toString(),
                        response.body()?.title, response.body()?.text, response.body()?.userId.toString(), response.body()?.id.toString())
                    binding.tvResponseCode.text = responseText
                }catch(e: Exception){
                    println(e.message)
                }
            }

            override fun onFailure(call: Call<PostResponse>, t: Throwable) {
                binding.tvResponseCode.text = t.message
            }

        })
    }

    private fun getUserPost(postId: Int){
        RetrofitClient.instance.getPostsByUser(postId).enqueue(object : Callback<ArrayList<PostResponse>>{
            override fun onResponse(call: Call<ArrayList<PostResponse>>, response: Response<ArrayList<PostResponse>>) {
                val responseCode = response.code().toString()
                //replace this with resource string placeholders
                binding.tvResponseCode.text = "Response code: $responseCode"
                response.body()?.let { list.addAll(it) }
                val adapter = PostAdapter(list, this@MainActivity)
                binding.rvRespnse.adapter = adapter
            }

            override fun onFailure(call: Call<ArrayList<PostResponse>>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    private fun getComment(commentId: Int){
        if (commentId > 0){
            RetrofitClient.instance.getComment(commentId).enqueue(object : Callback<ArrayList<CommentResponse>>{
                override fun onResponse(
                    call: Call<ArrayList<CommentResponse>>,
                    response: Response<ArrayList<CommentResponse>>
                ) {
                    binding.tvResponseCode.text = response.code().toString()
                    response.body()?.let{ commentList.addAll(it) }
                    try{
                        val commentAdapter = CommentAdapter(commentList, this@MainActivity)
                        binding.rvRespnse.adapter = commentAdapter
                    }catch(e: Exception){
                        println(e.message)
                    }
                }

                override fun onFailure(call: Call<ArrayList<CommentResponse>>, t: Throwable) {
                    binding.tvResponseCode.text = t.message
                }
            })
        }
    }

    private fun showComments(){
        RetrofitClient.instance.getCommets().enqueue(object : Callback<ArrayList<CommentResponse>>{
            override fun onResponse(
                call: Call<ArrayList<CommentResponse>>,
                response: Response<ArrayList<CommentResponse>>
            ) {
                binding.tvResponseCode.text = response.code().toString()
                response.body()?.let{ commentList.addAll(it) }
                try{
                    val commentAdapter = CommentAdapter(commentList, this@MainActivity)
                    binding.rvRespnse.adapter = commentAdapter
                }catch(e: Exception){
                    println(e.message)
                }
            }

            override fun onFailure(call: Call<ArrayList<CommentResponse>>, t: Throwable) {
                binding.tvResponseCode.text = t.message
            }
        })
    }

    private fun createPost(){
        /*we're gonna create the data to send by hand first
        * REMEMBER: the resource will not be created but it'll be faked as if.*/
        RetrofitClient.instance.createPost(
            29,
            "Retrofit value",
            "This is our first value send to the REST service"
        ).enqueue(object : Callback<CreatePostResponse> {
            override fun onResponse(
                call: Call<CreatePostResponse>,
                response: Response<CreatePostResponse>
            ) {
                val responseText = resources.getString(R.string.create_post_response, response.code().toString(),
                    response.body()?.title, response.body()?.text, response.body()?.userId, response.body()?.id.toString())
                binding.tvResponseCode.text = responseText
            }

            override fun onFailure(call: Call<CreatePostResponse>, t: Throwable) {
                binding.tvResponseCode.text = t.message
            }

        })
    }

    private fun getPosts(){
        RetrofitClient.instance.getPosts().enqueue(object : Callback<ArrayList<PostResponse>>{
            override fun onResponse(call: Call<ArrayList<PostResponse>>, response: Response<ArrayList<PostResponse>>) {
                val responseCode = response.code().toString()

                //replace this with resource string placeholders
                binding.tvResponseCode.text = "Response code: $responseCode"
                response.body()?.let { list.addAll(it) }
                val adapter = PostAdapter(list, this@MainActivity)
                binding.rvRespnse.adapter = adapter
            }

            override fun onFailure(call: Call<ArrayList<PostResponse>>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }
}