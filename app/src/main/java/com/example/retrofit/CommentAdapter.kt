package com.example.retrofit

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit.databinding.ItemPostBinding

class CommentAdapter(private val listComment: ArrayList<CommentResponse>, val context: Context): RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {
    inner class CommentViewHolder(private val commentBinding: ItemPostBinding): RecyclerView.ViewHolder(commentBinding.root){
        fun bind(commentResponse: CommentResponse){
            with(commentBinding){
                val text = context.resources.getString(R.string.comment_response_text, commentResponse.postId.toString(),
                    commentResponse.id.toString(), commentResponse.name, commentResponse.email, commentResponse.body)
                this.tvItemCardText.text = text
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val commentBinding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CommentViewHolder(commentBinding)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bind(listComment[position])
    }

    override fun getItemCount(): Int = listComment.size
}