package com.example.retrofit


import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.PUT
import retrofit2.http.PATCH
import retrofit2.http.DELETE

/*earch what is MIME type*/
interface Api {
    @GET("posts")
    fun getPosts(): Call<ArrayList<PostResponse>>

    @FormUrlEncoded
    @POST("posts")
    fun createPost(
        @Field("userId") userId: Int,
        @Field("title") title: String,
        @Field("body") body: String
    ): Call<CreatePostResponse>

    @GET("/posts/1/comments")
    fun getCommets(): Call<ArrayList<CommentResponse>>

    @GET("/posts/{id}/comments")
    fun getComment(@Path("id") postId: Int): Call<ArrayList<CommentResponse>>

    @GET("posts")
    fun getPostsByUser(@Query("userId") userId: Int): Call<ArrayList<PostResponse>>

    @FormUrlEncoded
    @PUT("posts/{id}")
    fun putPost(
        @Path("id") id: Int,
        @Field("userId") userId: Int,
        @Field("id") idField: Int,
        @Field("title") title: String?,
        @Field("body") text: String?,
    ): Call<PostResponse>

    @FormUrlEncoded
    @PATCH("posts/{id}")
    fun patchPost(
        @Path("id") id: Int,
        @Field("userId") userId: Int,
        @Field("id") idField: Int,
        @Field("title") title: String?,
        @Field("body") text: String?,
    ): Call<PostResponse>

    @DELETE("posts/{number}")
    fun deletePost(@Path("number") id: Int): Call<Void>
}