package com.example.retrofit

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit.databinding.ItemPostBinding

class PostAdapter(private val list: ArrayList<PostResponse>, val context: Context): RecyclerView.Adapter<PostAdapter.PostViewHolder>() {
    inner class PostViewHolder(private val itemBinding: ItemPostBinding): RecyclerView.ViewHolder(itemBinding.root){
        fun bind(postResponse: PostResponse){

            with(itemBinding){
                val text = context.resources.getString(R.string.get_posts, postResponse.userId.toString(), postResponse.id.toString(),
                    postResponse.title, postResponse.text)
                this.tvItemCardText.text = text
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val itemBinding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size
}